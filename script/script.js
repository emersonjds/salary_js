var btnCalc = document.querySelector("#calculate");
var btnClean = document.querySelector("#cleanFields");

var final_value = 0;
var resp = document.querySelector("#resp");

btnCalc.addEventListener("click", () => resp.value = salaryCalc());

btnClean.addEventListener("click", () => clear());

function salaryCalc() {
  var minimum_salary = document.getElementById("minimum_salary").value;
  var gross_salary = document.getElementById("gross_salary").value;
  final_value = gross_salary / minimum_salary;
  return final_value;
}

function clear() {
  document.getElementById("minimum_salary").value = "";
  document.getElementById("gross_salary").value = "";
  document.getElementById("resp").value = "";
}
